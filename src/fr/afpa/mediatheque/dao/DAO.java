package fr.afpa.mediatheque.dao;

import fr.afpa.mediatheque.utils.ConnectionPostgreSQL;

import java.sql.Connection;

public interface DAO<T> {
    public Connection connect = ConnectionPostgreSQL.getInstance();

    public  T find(long id)  ;

    public  void create(T obj);

    public  void update(T obj);

    public  void delete(T obj);


}
