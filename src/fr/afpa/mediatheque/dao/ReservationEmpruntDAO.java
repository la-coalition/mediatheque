package fr.afpa.mediatheque.dao;

import fr.afpa.mediatheque.utils.ConnectionPostgreSQL;

import java.sql.Connection;
import java.util.List;

public interface ReservationEmpruntDAO<T> {
    public Connection connect = ConnectionPostgreSQL.getInstance();

    public  T find(long id)  ;

    public  void create(T obj);

    public  void update(T obj);

    public  void delete(T obj);
    public  T findByIdOuvrage (long idOuvrage)  ;
    public List <T >finAlldByIdAdherent(long idAdherent)  ;

    List<T> findAllOutOfTime();
}
