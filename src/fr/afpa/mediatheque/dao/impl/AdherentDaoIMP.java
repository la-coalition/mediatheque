package fr.afpa.mediatheque.dao.impl;

import fr.afpa.mediatheque.dao.DAO;
import fr.afpa.mediatheque.dao.IAdherentDAO;
import fr.afpa.mediatheque.models.Adherent;
import fr.afpa.mediatheque.models.Oeuvre;
import fr.afpa.mediatheque.services.IAdherentService;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AdherentDaoIMP implements DAO<Adherent>, IAdherentDAO {
    @Override
    public Adherent find(long id) {
        Adherent adherent = new Adherent();
        try {
            ResultSet result = this.connect
                    .createStatement(
                    ).executeQuery(
                            "SELECT * FROM Adherent WHERE id_adherent = " + id
                    );

            while (result.next()){

                adherent.setId(result.getLong("id_adherent"));
                adherent.setNom(result.getString("nom"));
                adherent.setPrenom(result.getString("prenom"));
                adherent.setEmail(result.getString("email"));
                adherent.setUserPassword(result.getString("user_password"));
                adherent.setUserRole(result.getString("user_role"));
                adherent.setSolde(result.getDouble("solde"));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return adherent;
    }

    @Override
    public void create(Adherent obj) {
        String query = "INSERT INTO adherent(nom, prenom,email,user_password, user_role , solde) VALUES(?,?,?,?,?,?);";
        try {
            PreparedStatement prepare = this.connect.prepareStatement(query);
            prepare.setString(1, obj.getNom());
            prepare.setString(2, obj.getPrenom() );
            prepare.setString(3, obj.getEmail());
            prepare.setString(4, obj.getUserPassword());
            prepare.setString(5, obj.getUserRole());
            prepare.setDouble(6, obj.getSolde());

            prepare.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public void update(Adherent obj) {
        String query = "UPDATE adherent " +
                "SET nom = '"+obj.getNom()+"', " +
                "prenom = '"+obj.getPrenom()+"', " +
                "user_password = '" + obj.getUserPassword() + "', " +
                "solde = '" + obj.getSolde() + "' " +
                "WHERE email = '" + obj.getEmail() +"';";
        try {
            this.connect.createStatement().executeUpdate(query);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Adherent obj) {
        String query = "DELETE FROM adherent WHERE email='"+obj.getEmail()+"'";
        try {
            this.connect.createStatement().executeUpdate(query);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Adherent findByLogin(String login) {
        Adherent adherent = new Adherent();

        String query = "SELECT * FROM Adherent WHERE email = '" + login + "'";

        try {
            ResultSet result = this.connect.createStatement().executeQuery(query);

            while (result.next()){
                adherent.setId(result.getLong("id_adherent"));
                adherent.setNom(result.getString("nom"));
                adherent.setPrenom(result.getString("prenom"));
                adherent.setEmail(result.getString("email"));
                adherent.setUserPassword(result.getString("user_password"));
                adherent.setUserRole(result.getString("user_role"));
                adherent.setSolde(result.getDouble("solde"));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return adherent;
    }
}
