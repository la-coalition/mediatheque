package fr.afpa.mediatheque.dao.impl;

import fr.afpa.mediatheque.dao.IAppUserDAO;
import fr.afpa.mediatheque.models.Adherent;
import fr.afpa.mediatheque.models.AppUser;
import fr.afpa.mediatheque.dao.DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AppUserDaoIMP implements DAO<AppUser>, IAppUserDAO {
    @Override
    public AppUser find(long id) {
        return null;
    }

    @Override
    public void create(AppUser obj) {
        String query = "INSERT INTO app_user(nom, prenom ,email,user_password, user_role) VALUES(?,?,?,?,?);";
        try {
            PreparedStatement prepare = this.connect.prepareStatement(query);
            prepare.setString(1, obj.getNom());
            prepare.setString(2, obj.getPrenom());
            prepare.setString(3, obj.getEmail());
            prepare.setString(4, obj.getUserPassword());
            prepare.setString(5, obj.getUserRole());

            prepare.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void update(AppUser obj) {
        String query = "UPDATE app_user " +
                "SET" +
                "nom = '" + obj.getNom() +"'" +
                "prenom ='" + obj.getPrenom() + "'" +
                "email ='" + obj.getEmail() + "'" +
                "user_password ='" + obj.getUserPassword() + "'" +
                "user_role ='" + obj.getUserRole() + "'" +
                "WHERE email = '" + obj.getEmail() +"'";

        try {
            this.connect.createStatement().executeUpdate(query);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void delete(AppUser obj) {
        try {

            this.connect
                    .createStatement().executeUpdate("DELETE FROM App_User WHERE id_App_User = " + obj.getId());

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public AppUser findByLogin(String login) {

        AppUser appUser = new AppUser();

        String query = "SELECT * FROM app_user where email = '" + login + "';";

        try {

            ResultSet result = this.connect.createStatement().executeQuery(query);

            while (result.next()){

                appUser.setId(result.getLong("id_app_user"));
                appUser.setNom(result.getString("nom"));
                appUser.setPrenom(result.getString("prenom"));
                appUser.setEmail(result.getString("email"));
                appUser.setUserPassword(result.getString("user_password"));
                appUser.setUserRole(result.getString("user_role"));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return appUser;
    }

    @Override
    public List<Adherent> findAll() {
        List <Adherent> list = new ArrayList<>();
        String query = "SELECT id_adherent, nom, prenom, email, user_password, user_role, solde FROM adherent";
        Adherent adherent ;

        try {
            ResultSet results = this.connect.createStatement().executeQuery(query);
            while(results.next()) {
                adherent = new Adherent();
                adherent.setId(results.getLong("id_adherent"));
                adherent.setNom(results.getString("nom"));
                adherent.setPrenom(results.getString("prenom"));
                adherent.setEmail(results.getString("email"));
                adherent.setUserPassword(results.getString("user_password"));
                adherent.setUserRole(results.getString("user_role"));
                adherent.setSolde(results.getDouble("solde"));
                list.add(adherent);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return list;
    }
}
