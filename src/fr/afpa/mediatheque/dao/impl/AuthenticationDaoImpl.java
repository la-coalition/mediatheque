package fr.afpa.mediatheque.dao.impl;


import fr.afpa.mediatheque.dao.AuthenticationDao;
import fr.afpa.mediatheque.models.Adherent;
import fr.afpa.mediatheque.models.AppUser;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class AuthenticationDaoImpl implements AuthenticationDao {
    @Override
    public AppUser authenticate(String username, String password) throws Exception {
        // List<T> usersList =  (List<T>) FileUtils.<User>readFromFile(Constantes.USER_FILE);

        String query = "SELECT * FROM app_user WHERE email = '" + username + "' AND user_password = '" + password + "'";

        ResultSet results = this.connect.createStatement().executeQuery(query);
        List<AppUser> usersList = new ArrayList<>();

        while(results.next()) {
            AppUser appUser = new AppUser();
            appUser.setId(results.getLong("id_app_user"));
            appUser.setNom(results.getString("nom"));
            appUser.setPrenom(results.getString("prenom"));
            appUser.setEmail(results.getString("email"));
            appUser.setUserPassword(results.getString("user_password"));
            appUser.setUserRole(results.getString("user_role"));
            usersList.add(appUser);
        }

        AppUser appuser = usersList.stream()
                .filter(userElement -> userElement.getEmail().equals(username) && userElement.getUserPassword().equalsIgnoreCase(password))
                .findFirst()
                .orElse(null);

        if (Objects.isNull(appuser)) {
            throw new Exception("Identifiants incorrect");
        }

        return appuser;
    }

    public Adherent authenticateAdherent(String username, String password) {

        String query = "SELECT * FROM adherent WHERE email = '" + username + "' AND user_password = '" + password + "'";

        ResultSet results = null;

        List<Adherent> adherentList = new ArrayList<>();
        Adherent adherent ;
        try {
            results = this.connect.createStatement().executeQuery(query);

            while(results.next()) {
                adherent = new Adherent();
                adherent.setId(results.getLong("id_adherent"));
                adherent.setNom(results.getString("nom"));
                adherent.setPrenom(results.getString("prenom"));
                adherent.setEmail(results.getString("email"));
                adherent.setUserPassword(results.getString("user_password"));
                adherent.setUserRole(results.getString("user_role"));
                adherent.setSolde(results.getDouble("solde"));
                adherentList.add(adherent);
            }

            adherent = adherentList.stream()
                    .filter(userElement -> userElement.getEmail().equals(username) && userElement.getUserPassword().equalsIgnoreCase(password))
                    .findFirst()
                    .orElse(null);

            if (adherent==null) {
                try {
                    throw new Exception("Identifiants incorrect");
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return adherent;
    }
}
