package fr.afpa.mediatheque.dao.impl;

import fr.afpa.mediatheque.dao.DAO;
import fr.afpa.mediatheque.dao.ReservationEmpruntDAO;
import fr.afpa.mediatheque.models.Adherent;
import fr.afpa.mediatheque.models.Emprunt;
import fr.afpa.mediatheque.models.Ouvrage;
import fr.afpa.mediatheque.models.Reservation;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class EmpruntDaoIMP implements ReservationEmpruntDAO<Emprunt> {

    DAO<Ouvrage> ouvrageDAO= new OuvrageDaoIMP();
    DAO<Adherent> adherentDAO= new AdherentDaoIMP();
    @Override
    public Emprunt find(long id) {
        Emprunt emprunt = new Emprunt();
        try {
            ResultSet result = this.connect
                    .createStatement(
                    ).executeQuery(
                            "SELECT * FROM Emprunt WHERE id_emprunt = " + id
                    );

            while (result.next()){
                emprunt.setId(result.getLong("id_emprunt"));
                emprunt.setDateEmprunt(result.getDate("date_emprunt"));
                Ouvrage ouvrage = ouvrageDAO.find(result.getLong("id_ouvrage"));
                emprunt.setOuvrage(ouvrage);
                Adherent adherent = adherentDAO.find(result.getLong("id_adherent"));
                emprunt.setAdherent(adherent);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return emprunt;
    }

    @Override
    public void create(Emprunt obj) {
        String query = "INSERT INTO Emprunt(date_emprunt, id_ouvrage,id_adherent) VALUES(?,?,?);";
        try {
            PreparedStatement prepare = this.connect.prepareStatement(query);
            prepare.setDate(1, obj.getDateEmprunt());
            prepare.setLong(2, obj.getOuvrage().getId());
            prepare.setLong(3, obj.getAdherent().getId());

            prepare.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void update(Emprunt obj) {
        try {
            this.connect
                    .createStatement(

                    ).executeUpdate(
                            "UPDATE Emprunt " +
                                    "SET date_emprunt = '"+obj.getDateEmprunt()+"', " +
                                    "id_ouvrage = '"+obj.getOuvrage().getId()+"', " +
                                    "id_adherent= '" + obj.getAdherent().getId() + "' " +
                                    "WHERE id_emprunt  = '" + obj.getId() +"';"
                    );

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void delete(Emprunt obj) {
        try {

            this.connect
                    .createStatement(

                    ).executeUpdate(
                            "DELETE FROM Emprunt WHERE id_emprunt = " + obj.getId()
                    );

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public Emprunt findByIdOuvrage(long idOuvrage) {
        Emprunt emprunt = new Emprunt();
        try {
            ResultSet result = this.connect
                    .createStatement(
                    ).executeQuery(
                            "SELECT * FROM Emprunt WHERE id_ouvrage = " + idOuvrage
                    );

            while (result.next()){
                emprunt.setId(result.getLong("id_emprunt"));
                emprunt.setDateEmprunt(result.getDate("date_emprunt"));
                Ouvrage ouvrage = ouvrageDAO.find(result.getLong("id_ouvrage"));
                emprunt.setOuvrage(ouvrage);
                Adherent adherent = adherentDAO.find(result.getLong("id_adherent"));
                emprunt.setAdherent(adherent);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return emprunt;
    }

    @Override
    public List<Emprunt> finAlldByIdAdherent(long idAdherent) {
        List<Emprunt> empruntsAdherent = new ArrayList<>();
        try {
            ResultSet result = this.connect
                    .createStatement(
                    ).executeQuery(
                            "SELECT * FROM Emprunt WHERE id_adherent = " + idAdherent
                    );

            while (result.next()){
                Emprunt emprunt = new Emprunt();
                emprunt.setId(result.getLong("id_emprunt"));
                emprunt.setDateEmprunt(result.getDate("date_emprunt"));
                Ouvrage ouvrage = ouvrageDAO.find(result.getLong("id_ouvrage"));
                emprunt.setOuvrage(ouvrage);
                Adherent adherent = adherentDAO.find(result.getLong("id_adherent"));
                emprunt.setAdherent(adherent);
                empruntsAdherent.add(emprunt);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return empruntsAdherent;
    }

    @Override
    public List<Emprunt> findAllOutOfTime() {
        List<Emprunt> empruntsOutOfTime = new ArrayList<>();
        try {
            ResultSet result = this.connect
                    .createStatement(
                    ).executeQuery(
                            "SELECT * FROM Emprunt WHERE (current_date - date_emprunt) > 14 "
                    );

            while (result.next()){
                Emprunt emprunt = new Emprunt();
                emprunt.setId(result.getLong("id_emprunt"));
                emprunt.setDateEmprunt(result.getDate("date_emprunt"));
                Ouvrage ouvrage = ouvrageDAO.find(result.getLong("id_ouvrage"));
                emprunt.setOuvrage(ouvrage);
                Adherent adherent = adherentDAO.find(result.getLong("id_adherent"));
                emprunt.setAdherent(adherent);
                empruntsOutOfTime.add(emprunt);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return empruntsOutOfTime;
    }
}
