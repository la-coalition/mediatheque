package fr.afpa.mediatheque.dao.impl;

import fr.afpa.mediatheque.dao.DAO;
import fr.afpa.mediatheque.dao.IOeuvreDAO;
import fr.afpa.mediatheque.models.Oeuvre;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class OeuvreDaoIMP implements DAO<Oeuvre>, IOeuvreDAO {

    @Override
    public Oeuvre find(long id) {
        Oeuvre oeuvre = new Oeuvre();
        try {
            ResultSet result = this.connect
                    .createStatement(
                    ).executeQuery(
                            "SELECT * FROM Oeuvre WHERE id_oeuvre = " + id
                    );

            while (result.next()){

                oeuvre.setId(result.getLong("id_oeuvre"));
                oeuvre.setTitre(result.getString("titre"));
                oeuvre.setGenre(result.getString("genre"));
                oeuvre.setAnneeParution(result.getDate("annee_parution"));
                oeuvre.setExemplaires(result.getInt("exemplaires"));
                oeuvre.setExemplairesDispo(result.getInt("exemplaires_dispo"));
                oeuvre.setPrix(result.getDouble( "prix"));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return oeuvre;
    }

    @Override
    public void create(Oeuvre obj) {

        String query = "INSERT INTO Oeuvre(titre, genre,annee_parution,exemplaires, exemplaires_dispo , prix) VALUES(?,?,?,?,?,?);";
        try {
            PreparedStatement prepare = this.connect.prepareStatement(query);
            prepare.setString(1, obj.getTitre());
            prepare.setString(2, obj.getGenre());
            prepare.setDate(3, obj.getAnneeParution());
            prepare.setInt(4, obj.getExemplaires());
            prepare.setInt(5, obj.getExemplairesDispo());
            prepare.setDouble(6, obj.getPrix());

            prepare.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public void update(Oeuvre obj) {
        try {
            this.connect
                    .createStatement(

                    ).executeUpdate(
                            "UPDATE Oeuvre " +
                                    "SET titre = '"+obj.getTitre()+"', " +
                                    "genre = '"+obj.getGenre()+"', " +
                                    "annee_parution = '" + obj.getAnneeParution() + "', " +
                                    "exemplaires = '" + obj.getExemplaires() + "', " +
                                    "exemplaires_dispo = '" + obj.getExemplairesDispo() + "', " +
                                    "prix = '" + obj.getPrix() + "' " +
                                    "WHERE id_oeuvre = '" + obj.getId() +"';"
                    );

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void delete(Oeuvre obj) {
        deleteOuvrages(obj);
        try {
            this.connect
                    .createStatement(

                    ).executeUpdate(
                            "DELETE FROM Oeuvre WHERE id_oeuvre = " + obj.getId()
                    );

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteOuvrages(Oeuvre obj) {
        try {
            this.connect
                    .createStatement(

                    ).executeUpdate(
                            "DELETE FROM Ouvrage WHERE id_oeuvre = " + obj.getId()
                    );

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Override
    public Oeuvre findByTitle(String title) {
        Oeuvre oeuvre = new Oeuvre();
        try {
            ResultSet result = this.connect
                    .createStatement(
                    ).executeQuery(
                            "SELECT * FROM Oeuvre WHERE titre = '" + title + "'"
                    );

            while (result.next()){

                oeuvre.setId(result.getLong("id_oeuvre"));
                oeuvre.setTitre(result.getString("titre"));
                oeuvre.setGenre(result.getString("genre"));
                oeuvre.setAnneeParution(result.getDate("annee_parution"));
                oeuvre.setExemplaires(result.getInt("exemplaires"));
                oeuvre.setExemplairesDispo(result.getInt("exemplaires_dispo"));
                oeuvre.setPrix(result.getDouble( "prix"));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return oeuvre;
    }

    @Override
    public List<Oeuvre> findAndFilter(String[] keywords, List<String> filtres) {

        List<Oeuvre> list = new ArrayList<>();


        String keyword = "";

        for(int i =0; i < keywords.length; i++) {
            if(i < keywords.length-1) {
                keyword += "'%"+ keywords[i] + "%',";
            } else {
                keyword += "'%"+ keywords[i] + "%'";
            }
        }

        String selectedGenre = "";

        for(String filtre : filtres) {
            if(filtres.indexOf(Objects.toString(filtre)) < filtres.size()-1) {
                selectedGenre += "'" +filtre+"',";
            } else {
                selectedGenre += "'" +filtre+"'";
            }
        }

        System.out.println(selectedGenre);

        String query = "SELECT id_oeuvre, titre, genre, annee_parution, exemplaires, exemplaires_dispo, prix " +
                "FROM oeuvre " +
                "WHERE LOWER(titre) LIKE ANY (array["+keyword+"]) and genre in (" + selectedGenre + ");";


        //TODO : requête à vérifier sur psql
        System.out.println(query);

        try {
            ResultSet result = this.connect.createStatement().executeQuery(query);

            while (result.next()){
                Oeuvre oeuvre = new Oeuvre();
                oeuvre.setId(result.getLong("id_oeuvre"));
                oeuvre.setTitre(result.getString("titre"));
                oeuvre.setGenre(result.getString("genre"));
                oeuvre.setAnneeParution(result.getDate("annee_parution"));
                oeuvre.setExemplaires(result.getInt("exemplaires"));
                oeuvre.setExemplairesDispo(result.getInt("exemplaires_dispo"));
                oeuvre.setPrix(result.getDouble( "prix"));
                list.add(oeuvre);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return list;

    }

    public String concatFiltres(List<String> filtres) {

        String selectedGenre = "";

        for(String filtre : filtres) {
            if(filtres.indexOf(Objects.toString(filtre)) < filtres.size()-1) {
                selectedGenre += "'" +filtre+"',";
            } else {
                selectedGenre += "'" +filtre+"'";
            }
        }

        return selectedGenre;
    }
}
