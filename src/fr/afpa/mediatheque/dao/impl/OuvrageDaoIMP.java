package fr.afpa.mediatheque.dao.impl;

import fr.afpa.mediatheque.dao.DAO;
import fr.afpa.mediatheque.dao.IOuvrageDAO;
import fr.afpa.mediatheque.models.Oeuvre;
import fr.afpa.mediatheque.models.Ouvrage;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class OuvrageDaoIMP implements DAO<Ouvrage> , IOuvrageDAO {


    @Override
    public Ouvrage find(long id)  {
        DAO<Oeuvre> oeuvreDAO= new OeuvreDaoIMP();
        Ouvrage ouvrage = new Ouvrage();
        try {
            ResultSet result = this.connect
                    .createStatement(
                    ).executeQuery(
                            "SELECT * FROM Ouvrage WHERE id_ouvrage = " + id
                    );

            while (result.next()){

                ouvrage.setId(result.getLong("id_ouvrage"));
                ouvrage.setAvailable(result.getBoolean("disponible"));
                ouvrage.setRserved(result.getBoolean("reserve"));
                ouvrage.setToBeDeleted(result.getBoolean("a_supprimer"));
                Oeuvre oeuvre = oeuvreDAO.find(result.getLong("id_oeuvre"));
                ouvrage.setOeuvre(oeuvre);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ouvrage;
    }

    @Override
    public void create(Ouvrage obj) {
        String query = "INSERT INTO Ouvrage(disponible, reserve,a_supprimer, id_oeuvre) VALUES(?,?,?,?);";
        try {
            PreparedStatement prepare = this.connect.prepareStatement(query);
            prepare.setBoolean(1, obj.isAvailable());
            prepare.setBoolean(2, obj.isRserved());
            prepare.setBoolean(3, obj.isToBeDeleted());
            prepare.setLong(4, obj.getOeuvre().getId());

            prepare.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void update(Ouvrage obj) {
        try {
            this.connect
                    .createStatement(

                    ).executeUpdate(
                            "UPDATE Ouvrage " +
                                    "SET disponible = '"+obj.isAvailable()+"', " +
                                    "reserve = '"+obj.isRserved()+"', " +
                                    "a_supprimer= '" + obj.isToBeDeleted()  + "' ," +
                                    "id_oeuvre= '" + obj.getOeuvre().getId() + "' " +
                                    "WHERE id_ouvrage = '" + obj.getId() +"';"
                    );

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void delete(Ouvrage obj) {
        try {

            this.connect
                    .createStatement(

                    ).executeUpdate(
                            "DELETE FROM Ouvrage WHERE id_ouvrage = " + obj.getId()
                    );

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteOuvragesRendus() {
        try {
            this.connect
                    .createStatement(

                    ).executeUpdate(
                            "DELETE FROM Ouvrage WHERE  disponible=  true and a_supprimer=true ;"
                    );

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    public List<Long> findOuvrages()  {
        DAO<Oeuvre> oeuvreDAO= new OeuvreDaoIMP();
        List<Long> IdOeuvres = new ArrayList<>();
        try {
            ResultSet result = this.connect
                    .createStatement(
                    ).executeQuery(
                            "SELECT id_oeuvre FROM Ouvrage WHERE  disponible=  true and a_supprimer=true ;"
                    );

            while (result.next()){
               Long idOeuvre = result.getLong("id_oeuvre");
                IdOeuvres.add(idOeuvre);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return IdOeuvres;
    }

    @Override
    public List<Ouvrage> findOuvragesOeuvreDisponible(Long id) {
        DAO<Oeuvre> oeuvreDAO= new OeuvreDaoIMP();
        List<Ouvrage> ouvragesDispo = new ArrayList<>();
        try {
            ResultSet result = this.connect
                    .createStatement(
                    ).executeQuery(
                            "SELECT * FROM Ouvrage WHERE id_oeuvre= "+id +" and disponible=true and reserve=false;"
                    );

            while (result.next()){
                Ouvrage ouvrage = new Ouvrage();
                ouvrage.setId(result.getLong("id_ouvrage"));
                ouvrage.setAvailable(result.getBoolean("disponible"));
                ouvrage.setRserved(result.getBoolean("reserve"));
                ouvrage.setToBeDeleted(result.getBoolean("a_supprimer"));
                Oeuvre oeuvre = oeuvreDAO.find(result.getLong("id_oeuvre"));
                ouvrage.setOeuvre(oeuvre);
                ouvragesDispo.add(ouvrage);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ouvragesDispo;
    }
}
