package fr.afpa.mediatheque.dao.impl;

import fr.afpa.mediatheque.dao.DAO;
import fr.afpa.mediatheque.models.Oeuvre;
import fr.afpa.mediatheque.models.Personne;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PersonneDaoIMP implements DAO<Personne> {
    @Override
    public Personne find(long id) {
        Personne personne  = new Personne();
        try {
            ResultSet result = this.connect
                    .createStatement(
                    ).executeQuery(
                            "SELECT * FROM Personne WHERE id_personne = " + id
                    );

            while (result.next()){

                personne.setId(result.getLong("id_personne"));
                personne.setNom(result.getString("nom"));
                personne.setPrenom(result.getString("prenom"));
                personne.setProfession(result.getString("profession"));

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return personne;

    }

    @Override
    public void create(Personne obj) {
        String query = "INSERT INTO Personne( nom,  prenom,  profession) VALUES(?,?,?);";
        try {
            PreparedStatement prepare = this.connect.prepareStatement(query);
            prepare.setString(1, obj.getNom());
            prepare.setString(2, obj.getPrenom());
            prepare.setString(3, obj.getProfession());

            prepare.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public void update(Personne obj) {
        try {
            this.connect
                    .createStatement(

                    ).executeUpdate(
                            "UPDATE Personne " +
                                    "SET nom = '"+obj.getNom()+"', " +
                                    "prenom = '"+obj.getPrenom()+"', " +
                                    "profession= '" + obj.getProfession()  + "' " +
                                    "WHERE id_personne = '" + obj.getId() +"';"
                    );

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void delete(Personne obj) {
        try {

            this.connect
                    .createStatement(

                    ).executeUpdate(
                            "DELETE FROM Personne WHERE id_personne = " + obj.getId()
                    );

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
