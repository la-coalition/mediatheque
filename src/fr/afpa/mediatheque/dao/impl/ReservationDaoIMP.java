package fr.afpa.mediatheque.dao.impl;

import fr.afpa.mediatheque.dao.DAO;
import fr.afpa.mediatheque.dao.ReservationEmpruntDAO;
import fr.afpa.mediatheque.models.Adherent;
import fr.afpa.mediatheque.models.Emprunt;
import fr.afpa.mediatheque.models.Ouvrage;
import fr.afpa.mediatheque.models.Reservation;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ReservationDaoIMP implements ReservationEmpruntDAO<Reservation> {
    DAO<Ouvrage> ouvrageDAO= new OuvrageDaoIMP();
    DAO<Adherent> adherentDAO= new AdherentDaoIMP();
    @Override
    public Reservation find(long id) {

        Reservation reservation = new Reservation();
        try {
            ResultSet result = this.connect
                    .createStatement(
                    ).executeQuery(
                            "SELECT * FROM Reservation WHERE id_reservation = " + id
                    );
            while (result.next()){

                reservation.setId(result.getLong("id_reservation"));
                reservation.setDateReservation(result.getDate("date_reservation"));
                Ouvrage ouvrage = ouvrageDAO.find(result.getLong("id_ouvrage"));
                reservation.setOuvrage(ouvrage);
                Adherent adherent = adherentDAO.find(result.getLong("id_adherent"));
                reservation.setAdherent(adherent);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return reservation;
    }

    @Override
    public void create(Reservation obj) {
        String query = "INSERT INTO Reservation(date_reservation, id_ouvrage,id_adherent) VALUES(?,?,?);";
        try {
            PreparedStatement prepare = this.connect.prepareStatement(query);
            prepare.setDate(1, obj.getDateReservation());
            prepare.setLong(2, obj.getOuvrage().getId());
            prepare.setLong(3, obj.getAdherent().getId());

            prepare.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void update(Reservation obj) {
        try {
            this.connect
                    .createStatement(

                    ).executeUpdate(
                            "UPDATE Reservation " +
                                    "SET date_reservation = '"+obj.getDateReservation()+"', " +
                                    "id_ouvrage = '"+obj.getOuvrage().getId()+"', " +
                                    "id_adherent= '" + obj.getAdherent().getId() + "' " +
                                    "WHERE id_reservation  = '" + obj.getId() +"';"
                    );

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void delete(Reservation obj) {
        try {

            this.connect
                    .createStatement(

                    ).executeUpdate(
                            "DELETE FROM Reservation WHERE id_reservation = " + obj.getId()
                    );

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public Reservation findByIdOuvrage(long idOuvrage) {

        Reservation reservation = new Reservation();
        try {
            ResultSet result = this.connect
                    .createStatement(
                    ).executeQuery(
                            "SELECT * FROM Reservation WHERE id_ouvrage = " + idOuvrage
                    );

            while (result.next()){
                reservation.setId(result.getLong("id_reservation"));
                reservation.setDateReservation(result.getDate("date_reservation"));
                Ouvrage ouvrage = ouvrageDAO.find(result.getLong("id_ouvrage"));
                reservation.setOuvrage(ouvrage);
                Adherent adherent = adherentDAO.find(result.getLong("id_adherent"));
                reservation.setAdherent(adherent);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return reservation;
    }

    @Override
    public List<Reservation>  finAlldByIdAdherent(long idAdherent) {

        List<Reservation> reservationsAdherent = new ArrayList<>();
        try {
            ResultSet result = this.connect
                    .createStatement(
                    ).executeQuery(
                            "SELECT * FROM Reservation WHERE id_adherent = " + idAdherent
                    );

            while (result.next()){
                Reservation reservation = new Reservation();
                reservation.setId(result.getLong("id_reservation"));
                reservation.setDateReservation(result.getDate("date_reservation"));
                Ouvrage ouvrage = ouvrageDAO.find(result.getLong("id_ouvrage"));
                reservation.setOuvrage(ouvrage);
                Adherent adherent = adherentDAO.find(result.getLong("id_adherent"));
                reservation.setAdherent(adherent);
                reservationsAdherent.add(reservation);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return reservationsAdherent;
    }

    @Override
    public List<Reservation> findAllOutOfTime() {
        List<Reservation> reservationsOutOfTime = new ArrayList<>();
        try {
            ResultSet result = this.connect
                    .createStatement(
                    ).executeQuery(
                            "SELECT * FROM Reservation WHERE (current_date -date_reservation) > 7"
                    );

            while (result.next()){
                Reservation reservation = new Reservation();
                reservation.setId(result.getLong("id_reservation"));
                reservation.setDateReservation(result.getDate("date_reservation"));
                Ouvrage ouvrage = ouvrageDAO.find(result.getLong("id_ouvrage"));
                reservation.setOuvrage(ouvrage);
                Adherent adherent = adherentDAO.find(result.getLong("id_adherent"));
                reservation.setAdherent(adherent);
                reservationsOutOfTime.add(reservation);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return reservationsOutOfTime;
    }
}
