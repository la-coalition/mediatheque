package fr.afpa.mediatheque.models;

import fr.afpa.mediatheque.utils.Constantes;

public class Adherent extends AppUser{

    private Double solde;

    public Adherent() {
        this.solde = 00.00;
    }

    public Adherent(String nom, String prenom, String email, String userPassword) {
        super(nom, prenom, email, userPassword);
        super.setUserRole(Constantes.ROLE_ADHERENT);
        this.solde = 00.00;
    }

    public Adherent(String nom, String prenom, String email, String userPassword, String adherent, Double solde) {
        super(nom, prenom, email, userPassword);
        userRole= Constantes.ROLE_ADHERENT;
        this.solde = solde;
    }

    public Double getSolde() {
        return solde;
    }

    public void setSolde(Double solde) {
        this.solde = solde;
    }

    @Override
    public String toString() {
        return "AppUser{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", email='" + email + '\'' +
                ", userPassword='" + userPassword + '\'' +
                ", userRole='" + userRole + '\'' +
                ", solde='" + solde + '\'' +

                '}';
    }
}
