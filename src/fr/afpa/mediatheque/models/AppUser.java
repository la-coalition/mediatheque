package fr.afpa.mediatheque.models;

public  class AppUser {

    protected long id;
    protected String nom;
    protected String prenom;
    protected String email;
    protected String userPassword;
    protected String userRole;

    public AppUser() {
    }

    public AppUser(String nom, String prenom, String email, String userPassword, String userRole) {
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.userPassword = userPassword;
        this.userRole = userRole;
    }

    public AppUser(String nom, String prenom, String email, String userPassword) {
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.userPassword = userPassword;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }


    @Override
    public String toString() {
        return "AppUser{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", email='" + email + '\'' +
                ", userPassword='" + userPassword + '\'' +
                ", userRole='" + userRole + '\'' +
                '}';
    }
}
