package fr.afpa.mediatheque.models;

import java.sql.Date;

public class Emprunt {
    private Long id;
    private Date dateEmprunt;
    private Ouvrage ouvrage ;
    private Adherent adherent;


    public Emprunt(Date dateEmprunt, Ouvrage ouvrage, Adherent adherent) {
        this.dateEmprunt = dateEmprunt;
        this.ouvrage = ouvrage;
        this.adherent = adherent;
    }

    public Emprunt() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDateEmprunt() {
        return dateEmprunt;
    }

    public void setDateEmprunt(Date dateEmprunt) {
        this.dateEmprunt = dateEmprunt;
    }

    public Ouvrage getOuvrage() {
        return ouvrage;
    }

    public void setOuvrage(Ouvrage ouvrage) {
        this.ouvrage = ouvrage;
    }

    public Adherent getAdherent() {
        return adherent;
    }

    public void setAdherent(Adherent adherent) {
        this.adherent = adherent;
    }

    @Override
    public String toString() {
        return "Emprunt{" +
                "id=" + id +
                ", dateEmprunt=" + dateEmprunt +
                ", ouvrage=" + ouvrage +
                ", adherent=" + adherent +
                '}';
    }
}
