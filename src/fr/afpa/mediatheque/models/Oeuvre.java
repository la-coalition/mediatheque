package fr.afpa.mediatheque.models;


import fr.afpa.mediatheque.utils.Constantes;

import java.sql.Date;
import java.time.LocalDate;
import java.time.temporal.TemporalAccessor;

public  class Oeuvre {
    private Long id;
    private String titre;
    private String genre;

    private Date anneeParution;
    private Integer exemplaires;

    private Integer exemplairesDispo;

    private Double prix;

    public Oeuvre() {
    }

    public Oeuvre(String genre, Double prix) {
        this.genre = genre;
        this.prix = prix;
    }


    public Oeuvre(String titre, String genre, Date anneeParution, Integer exemplaires, Double prix) {
        this.titre = titre;
        this.genre = genre;
        this.anneeParution = anneeParution;
        this.exemplaires = exemplaires;
        this.exemplairesDispo = exemplaires;
        this.prix = prix;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }


    public Double getPrix() {
        return prix;
    }

    public void setPrix(Double prix) {
        this.prix = prix;
    }

    public Date getAnneeParution() {
        return anneeParution;
    }

    public void setAnneeParution(Date anneeParution) {
        this.anneeParution = anneeParution;
    }

    public Integer getExemplaires() {
        return exemplaires;
    }

    public void setExemplaires(Integer exemplaires) {
        this.exemplaires = exemplaires;
    }

    public Integer getExemplairesDispo() {
        return exemplairesDispo;
    }

    public void setExemplairesDispo(Integer exemplairesDispo) {
        this.exemplairesDispo = exemplairesDispo;
    }

    @Override
    public String toString() {
        return "Oeuvre{" +
                "id=" + id +
                ", titre='" + titre + '\'' +
                ", genre='" + genre + '\'' +
                ", anneeParution=" + Constantes.formatter.format( anneeParution.toLocalDate()) +
                ", exemplaires=" + exemplaires +
                ", exemplairesDispo=" + exemplairesDispo +
                ", prix=" + prix +
                '}';
    }
}
