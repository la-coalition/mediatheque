package fr.afpa.mediatheque.models;

import fr.afpa.mediatheque.utils.Constantes;

import java.sql.Date;

public class Ouvrage {

    private Long id;
    private Boolean isAvailable;
    private Boolean isRserved;
    private Boolean isToBeDeleted;
    private Oeuvre oeuvre;

    public Ouvrage() {

    }


    public Ouvrage(Oeuvre oeuvre) {
        this.isAvailable = true;
        this.isRserved = false;
        this.isToBeDeleted= false;
        this.oeuvre = oeuvre;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }

    public boolean isRserved() {
        return isRserved;
    }

    public void setRserved(boolean rserved) {
        isRserved = rserved;
    }

    public boolean isToBeDeleted() {
        return isToBeDeleted;
    }

    public void setToBeDeleted(boolean toBeDeleted) {
        isToBeDeleted = toBeDeleted;
    }

    public Oeuvre getOeuvre() {
        return oeuvre;
    }

    public void setOeuvre(Oeuvre oeuvre) {
        this.oeuvre = oeuvre;
    }

    @Override
    public String toString() {
        return "Ouvrage{" +
                "id=" + id +
                ", isAvailable=" + isAvailable +
                ", isRserved=" + isRserved +
                ", oeuvre=" + oeuvre +
                '}';
    }


}
