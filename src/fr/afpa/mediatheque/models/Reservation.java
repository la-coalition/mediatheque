package fr.afpa.mediatheque.models;

import fr.afpa.mediatheque.utils.Constantes;

import java.sql.Date;
import java.util.Calendar;

public class Reservation {
    private Long id;
    private Date dateReservation;
    private Ouvrage ouvrage ;
    private Adherent adherent;

    public Reservation( Date dateReservation, Ouvrage ouvrage, Adherent adherent) {
        this.dateReservation = dateReservation;
        this.ouvrage = ouvrage;
        this.adherent = adherent;
    }
    public Reservation() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDateReservation() {
        return dateReservation;
    }

    public void setDateReservation(Date dateReservation) {
        this.dateReservation = dateReservation;
    }

    public Ouvrage getOuvrage() {
        return ouvrage;
    }

    public void setOuvrage(Ouvrage ouvrage) {
        this.ouvrage = ouvrage;
    }

    public Adherent getAdherent() {
        return adherent;
    }

    public void setAdherent(Adherent adherent) {
        this.adherent = adherent;
    }

    @Override
    public String toString() {
        return "Reservation{" +
                "id=" + id +
                ", dateReservation=" + Constantes.formatter.format(dateReservation.toLocalDate()) +"\n"+
                ", ouvrage=" + ouvrage +"\n"+
                ", adherent=" + adherent +"\n"+
                '}';
    }
}
