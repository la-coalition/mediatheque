package fr.afpa.mediatheque.services;


import fr.afpa.mediatheque.models.Adherent;
import fr.afpa.mediatheque.models.AppUser;

public interface AuthenticationService {
    public AppUser authenticate(String email, String userPassword) throws Exception;

    public Adherent authenticateAdherent(String email, String userPassword);
}
