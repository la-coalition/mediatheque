package fr.afpa.mediatheque.services.impl;

import fr.afpa.mediatheque.dao.DAO;
import fr.afpa.mediatheque.dao.IAdherentDAO;
import fr.afpa.mediatheque.dao.impl.AdherentDaoIMP;
import fr.afpa.mediatheque.models.Adherent;
import fr.afpa.mediatheque.services.IAdherentService;

public class AdherentServiceIMP implements IAdherentService {

    DAO<Adherent> adherentDao = new AdherentDaoIMP();
    IAdherentDAO iadherent = new AdherentDaoIMP();


    @Override
    public Adherent findByLogin(String login) {
        return iadherent.findByLogin(login);
    }

    @Override
    public void create(Adherent adherent) {
        adherentDao.create(adherent);
    }

    @Override
    public void update(Adherent adherent) {
        adherentDao.update(adherent);
    }

    @Override
    public void deleteAccount(Adherent adherent) {
        adherentDao.delete(adherent);
    }
}
