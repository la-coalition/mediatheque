package fr.afpa.mediatheque.services.impl;

import fr.afpa.mediatheque.dao.DAO;
import fr.afpa.mediatheque.dao.IAppUserDAO;
import fr.afpa.mediatheque.dao.impl.AppUserDaoIMP;
import fr.afpa.mediatheque.models.Adherent;
import fr.afpa.mediatheque.models.AppUser;
import fr.afpa.mediatheque.services.IAdherentService;
import fr.afpa.mediatheque.services.IAppUserService;

import java.util.List;

public class AppUserService implements IAppUserService {

    DAO<AppUser> appUserDAO = new AppUserDaoIMP();
    IAppUserDAO iappUser = new AppUserDaoIMP();

    @Override
    public AppUser findByLogin(String login) {
        return iappUser.findByLogin(login);
    }

    @Override
    public void create(AppUser appUser) {
        appUserDAO.create(appUser);
    }

    @Override
    public void update(AppUser appUser) {
        appUserDAO.update(appUser);
    }

    @Override
    public void deleteAccount(AppUser appUser) {
        appUserDAO.delete(appUser);
    }

    public List<Adherent> getAll() {

        return iappUser.findAll();
    }
}
