package fr.afpa.mediatheque.services.impl;

import fr.afpa.mediatheque.dao.DAO;
import fr.afpa.mediatheque.dao.IOeuvreDAO;
import fr.afpa.mediatheque.dao.impl.OeuvreDaoIMP;
import fr.afpa.mediatheque.models.Oeuvre;
import fr.afpa.mediatheque.services.IOeuvreService;

import java.util.List;

public class OeuvreServiceIMP implements IOeuvreService {

    DAO<Oeuvre> oeuvreDao = new OeuvreDaoIMP();
    IOeuvreDAO oeuvreDao2 = new OeuvreDaoIMP();
    @Override
    public void create(Oeuvre oeuvre) {
        oeuvreDao.create(oeuvre);
    }

    @Override
    public void update(Oeuvre oeuvre) {
        oeuvreDao.update(oeuvre);
    }

    @Override
    public void delete(Oeuvre oeuvre) {
        oeuvreDao.delete(oeuvre);
    }

    @Override
    public Oeuvre find(Long id) {
        return oeuvreDao.find(id);
    }

    @Override
    public Oeuvre findByTitle(String title) {
        return oeuvreDao2.findByTitle(title);
    }

   @Override
    public List<Oeuvre> findAllByKeywords(String[] keywords, List<String> filtres) {
        return oeuvreDao2.findAndFilter(keywords, filtres);
    }

    public void updateGrowExemplaires(Oeuvre oeuvre) {
        oeuvre.setExemplaires(oeuvre.getExemplaires() +1);
        oeuvre.setExemplairesDispo(oeuvre.getExemplairesDispo() +1);
    }
    public void updateLessExemplaires(Oeuvre oeuvre) {
        oeuvre.setExemplaires(oeuvre.getExemplaires() -1);
        oeuvre.setExemplairesDispo(oeuvre.getExemplairesDispo() - 1);
    }
}
