package fr.afpa.mediatheque.services.impl;

import fr.afpa.mediatheque.dao.DAO;
import fr.afpa.mediatheque.dao.IOuvrageDAO;
import fr.afpa.mediatheque.dao.impl.OuvrageDaoIMP;
import fr.afpa.mediatheque.models.Oeuvre;
import fr.afpa.mediatheque.models.Ouvrage;
import fr.afpa.mediatheque.services.IOuvrageService;

import java.util.List;

public class OuvrageServiceIMP implements IOuvrageService {
    DAO<Ouvrage> ouvrageDAO= new OuvrageDaoIMP();
    IOuvrageDAO ouvrageDAO2= new OuvrageDaoIMP();

    public void createOuvrages(Oeuvre createdOeuvre) {
        Ouvrage ouvrage = new Ouvrage(createdOeuvre);
        for (int i=0; i<createdOeuvre.getExemplaires(); i++){
            ouvrageDAO.create(ouvrage);
        }
    }

    @Override
    public void create(Oeuvre oeuvre) {
        Ouvrage ouvrage = new Ouvrage(oeuvre);
        ouvrageDAO.create(ouvrage);
    }

    @Override
    public void delete(Ouvrage ouvrage) {
        ouvrageDAO.delete(ouvrage);
    }

    @Override
    public Ouvrage find(Long id) {
        return ouvrageDAO.find(id);
    }

    @Override
    public void deleteOuvragesRendus() {
        ouvrageDAO2.deleteOuvragesRendus();
    }

    @Override
    public List<Long> findOeuvreToUpDate() {
        return ouvrageDAO2.findOuvrages();
    }

    @Override
    public List<Ouvrage> findOuvrageDisponibles(Ouvrage ouvrage) {
       return  ouvrageDAO2.findOuvragesOeuvreDisponible(ouvrage.getOeuvre().getId());
    }

}
