package fr.afpa.mediatheque.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionPostgreSQL {

    //private static String url = "jdbc:postgresql://localhost:5433/mediatheque";
    private static String url = "jdbc:postgresql://localhost:5432/mediatheque";
    private static String user = "postgres";

    private static String passwd = "root";
    //private static String passwd = "2022SQLbouchra";
    //private static String passwd = "afpa2020";

    private static Connection connect;

    public static Connection getInstance(){
        if(connect == null){
            try {
                connect = DriverManager.getConnection(url, user, passwd);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return connect;
    }
}
