package fr.afpa.mediatheque.views;

import fr.afpa.mediatheque.controllers.AdherentController;
import fr.afpa.mediatheque.models.Adherent;
import fr.afpa.mediatheque.utils.Constantes;

public class AdherentView {

    //AdherentController adherentController = new AdherentController();

    public Adherent promptAdherant() {

        System.out.println("Nom : ");
        String nom = AppView.scanner.nextLine();

        System.out.println("Prenom : ");
        String prenom = AppView.scanner.nextLine();

        System.out.println("Entrez une adresse mail : ");
        String email = AppView.scanner.nextLine();

        System.out.println("Créer un mot de passe : ");
        String mdp = AppView.scanner.nextLine();

        return new Adherent(nom, prenom, email, mdp);
    }

    public String promptLogin() {
        System.out.println("Entrer le login : ");
        String login = AppView.scanner.nextLine();
        return login;
    }

    public Adherent promptUpdate(Adherent adherent) {
        String choice = "";

        do {
            choice = menuUpadte();

            switch(choice) {
                case "1" : {
                    System.out.println("Modifier le nom : ");
                    adherent.setNom(AppView.scanner.nextLine().trim());
                    break;
                }

                case "2" : {
                    System.out.println("Modifier le prenom : ");
                    adherent.setPrenom(AppView.scanner.nextLine().trim());
                    break;
                }

                case "3" : {
                    System.out.println("Entrez le nouveau mot de passe : ");
                    adherent.setUserPassword(AppView.scanner.nextLine().trim());
                    break;
                }

                case "0":{
                    break;
                }

                default: {
                    System.out.println("Mauvaise manip !");
                    break;
                }
            }

            System.out.println("Appuyez sur une touche pour continuer...");
            AppView.scanner.nextLine();

        } while(!choice.equalsIgnoreCase("0"));

        return adherent;
    }

    private String menuUpadte() {
        String choice = "";
        System.out.println("[1] - Pour modifier le nom");
        System.out.println("[2] - Pour modifier le prenom");
        System.out.println("[3] - Pour modifier le mot de passe");
        System.out.println();
        System.out.println("[0] - Pour envoyer les modifications");
        choice = AppView.scanner.nextLine();
        return choice;
    }

    public Adherent signIn() {
        System.out.println("Email : ");
        String email = AppView.scanner.nextLine();

        System.out.println("Entrer le mot de passe : ");
        String mdp = AppView.scanner.nextLine();

        Adherent adherent = new Adherent();
        adherent.setEmail(email);
        adherent.setUserPassword(mdp);
        return adherent;
    }

    private Adherent findByLogin(String email) {
        Adherent adherent = new Adherent();
        adherent.setEmail(email);
        return adherent;
    }

    public void display(Adherent adherent) {
        System.out.println(adherent.toString());
    }

    public String mainMenu() {
        String choice = "";

        System.out.println("Documentations");
        System.out.println("[1] - Rechercher une oeuvre");
        System.out.println("[2] - Reserver une ouvrage");
        System.out.println("[3] - Emprunter une ouvrage");
        System.out.println("[4] - Rendre une oeuvre (+ si en voie de sup => sup)");
        System.out.println();
        System.out.println("Mon compte");
        System.out.println("[5] - Voir mes informations personnelles");
        System.out.println("[6] - Modifier mes informations personnelles");
        System.out.println("[7] - Reapprovisionner mon solde");
        System.out.println("[8] - Supprimer mon compte");
        System.out.println();
        System.out.println("[0] - Se déconnecter");

        choice = AppView.scanner.nextLine();
        return choice;
    }

    public void switchChoice(String choice, AdherentController adherentController) {

        switch (choice) {
            case"1": {
                System.out.println("Rechercher une oeuvre");
                adherentController.searchOeuvre();
                break;
            }
            case "2": {
                System.out.println("Reserver un ouvrage");
                break;
            }
            case "3": {
                System.out.println("Emprunter une ouvrage");
                break;

            }
            case "4": {
                System.out.println("Rendre une oeuvre (+ si en voie de sup => sup)");
                break;
            }
            case "5": {
                System.out.println("Voir mes informations personnelles");
                adherentController.showAccount();
                break;
            }

            case "6": {
                System.out.println("Modifier mes informations personnelles");
                adherentController.updateAccount();
                break;
            }

            case "7": {
                System.out.println("Reapprovisionner mon solde");
                //adherentController.reloadSolde();
                break;
            }

            case "8": {
                System.out.println("Supprimer mon compte");
                adherentController.deleteAccount();
                break;
            }

            case "0": {
                System.out.println("Fermeture de l'application...");
                break;
            }

            default: {
                System.out.println("Mauvaise manip, recommences");
                break;
            }
        }

        if(!choice.equalsIgnoreCase("0")) {
            System.out.print("Appuyer sur une touche pour continuer...");
            AppView.scanner.nextLine();
        }
    }
}
