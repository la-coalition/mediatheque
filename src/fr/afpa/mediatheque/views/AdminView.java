package fr.afpa.mediatheque.views;

import fr.afpa.mediatheque.controllers.AdminController;
import fr.afpa.mediatheque.controllers.MediathecaireController;
import fr.afpa.mediatheque.models.Adherent;
import fr.afpa.mediatheque.models.AppUser;
import fr.afpa.mediatheque.utils.Constantes;

public class AdminView {

    public String mainMenu() {

        String choice = "";

        System.out.println("[1] - Créer un nouveau compte utilisateur");
        System.out.println();
        System.out.println("[0] - Quitter l'application");
        System.out.println();
        choice = AppView.scanner.nextLine();
        return choice;
    }

    public AppUser promptAppUser() {

        System.out.println("Nom : ");
        String nom = AppView.scanner.nextLine();

        System.out.println("Prenom : ");
        String prenom = AppView.scanner.nextLine();

        System.out.println("Entrez une adresse mail : ");
        String email = AppView.scanner.nextLine();

        System.out.println("Créer un mot de passe : ");
        String mdp = AppView.scanner.nextLine();

        System.out.println("Selectionner un role : ");
        System.out.println("[1] - Admin");
        System.out.println("[2] - Mediathecaire");
        String choiceRole = AppView.scanner.nextLine();
        String role = choiceRole.equals("1") ? Constantes.ROLE_ADMIN : Constantes.ROLE_MEDIATHECAIRE;

        return new AppUser(nom, prenom, email, mdp, role);
    }

    public void switchRole() {

    }

    public void switchChoice(String choice, AdminController adminController) {

        switch (choice) {
            case "1": {
                System.out.println("Créer un nouveau compte utilisateur");
                adminController.createAppUser();
                break;
            }

            case "0": {
                System.out.println("Fermeture de l'application...");
                break;
            }

            default: {
                System.out.println("Mauvaise manip, recommences");
                break;
            }
        }

        if(!choice.equalsIgnoreCase("0")) {
            System.out.print("Appuyer sur une touche pour continuer...");
            AppView.scanner.nextLine();
        }
    }
}
