package fr.afpa.mediatheque.views;
import fr.afpa.mediatheque.controllers.AppController;
import fr.afpa.mediatheque.utils.Constantes;
import java.sql.Date;
import fr.afpa.mediatheque.controllers.MediathecaireController;
import java.util.Scanner;

public class AppView {

    public static Scanner scanner = new Scanner(System.in);

    public String mainMenu() {

        String choice = "";

        System.out.println("[1] - Rechercher une oeuvre");
        System.out.println("[2] - S'inscrire");
        System.out.println("[3] - Se connecter");
        System.out.println();

        System.out.println("[0] - Quitter l'application");

        choice = scanner.nextLine().trim();
        return choice;
    }

    public void switchChoice(String choice, AppController appController) {

        switch (choice) {
            case "1": {
                System.out.println("Rechercher une oeuvre");
                appController.searchAllOeuvres();
                break;
            }

            case "2": {
                System.out.println("S'inscrire");
                appController.createAccount();
                break;
            }

            case "3": {
                System.out.println("Se connecter");
                appController.signIn();
                break;
            }


            case "0": {
                System.out.println("Fermeture de l'application...");
                break;
            }

            default: {
                System.out.println("Mauvaise manip, recommences");
                break;
            }
        }

        if(!choice.equalsIgnoreCase("0")) {
            System.out.print("Appuyer sur une touche pour continuer...");
            AppView.scanner.nextLine();
        }
    }


    public String afficheDateIfNull(Date date ) {
        String message;
        if (date != null) {
            message = Constantes.formatter.format(date.toLocalDate());
        } else {
            message = "null";
        }
        return message;
    }
    public Long promptID(String message) {

        System.out.println("Saisir l'ID "+ message +" : ");
        Long id = Long.parseLong(AppView.scanner.nextLine());
        return id;
    }

}
