package fr.afpa.mediatheque.views;

import fr.afpa.mediatheque.controllers.MediathecaireController;
import fr.afpa.mediatheque.models.Adherent;
import fr.afpa.mediatheque.models.Emprunt;
import fr.afpa.mediatheque.models.Ouvrage;
import fr.afpa.mediatheque.models.Reservation;
import fr.afpa.mediatheque.utils.Constantes;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

public class MediathecaireView {

    public String mainMenu() {
        String choice = "";

        System.out.println("\t Gestion des adhérants");
        System.out.println("[1] - Créer un adhérent");
        System.out.println("[2] - Consulter les adhérents");
        System.out.println("[3] - Supprimer un adhérent");
        System.out.println();

        System.out.println("\t Gestion des fonds");
        System.out.println( "\t\t   Oeuvre");
        System.out.println("[4] - Ajouter une oeuvre");
        System.out.println("[5] - Modifier une oeuvre");
        System.out.println("[6] - Supprimer une oeuvre");
        System.out.println();

        System.out.println("\t\t Gestion des ouvrages");
        System.out.println("[7] - Ajouter un ouvrage");
        System.out.println("[8] - Supprimer un ouvrage");
        System.out.println("[9] - Supprimer les ouvrages rendues en voie de suppression  ");
        System.out.println();

        System.out.println("\t\t Gestion des emprunts");
        System.out.println("[10] - Enregistrer un emprunt");
        System.out.println("[11] - Restituer un emprunt" );
        System.out.println();
        System.out.println("[12] - Enregistrer une réservation" );
        System.out.println("[13] - Restituer une réservation" );
        System.out.println();

        System.out.println("\t\t Gestion des réservations");
        System.out.println("[14] - Afficher toutes les réservations d'un adherent" );
        System.out.println("[15] - Afficher toutes les emprunts d'un adherent" );
        System.out.println();
        System.out.println("[16] - Afficher les réservation hors délai" );
        System.out.println("[17] - Afficher les emprunts hors délai" );
        System.out.println("[18] - Restituer les réservation hors délai" );
        System.out.println();

        choice = AppView.scanner.nextLine();
        return choice;
    }

    public void switchChoice(String choice, MediathecaireController mediathecaireController) {

        switch (choice) {

            case "1": {
                System.out.println("Créer un adhérent");
                mediathecaireController.createAdherent();
                break;
            }

            case "2": {
                System.out.println("Consulter les adhérents");
                mediathecaireController.showAllAdherants();
                break;
            }

            case "3": {
                System.out.println("Supprimer un adhérent");
                mediathecaireController.deleteAdherant();
                break;
            }

            case "4": {
                System.out.println("créer une oeuvre");
                mediathecaireController.createOeuvre();
                break;
            }

            case "5": {
                System.out.println("Modifier une oeuvre");
                mediathecaireController.updateOeuvre();
                break;
            }

            case "6": {
                System.out.println("Supprimer une oeuvre");
                mediathecaireController.deleteOeuvre();
                break;
            }
            case "7": {
                System.out.println("créer un ouvrage");
                mediathecaireController.createOuvrage();
                break;
            }
            case "8": {
                System.out.println("créer un ouvrage");
                mediathecaireController.deleteOuvrage();
                break;
            }
            case "9": {
                System.out.println("Supprimer les ouvrages en voie de suppression rendues ");
                mediathecaireController.deleteOuvragesRendus();
                break;
            }
            case "10": {
                System.out.println("Enregistrer un emprunt");
                mediathecaireController.saveEmprunt();
                break;
            }
            case "11": {
                System.out.println("Restituer un emprunt ");
                mediathecaireController.deleteEmprunt();
                break;
            }
            case "12": {
                System.out.println("Enregistrer une réservations");
                mediathecaireController.saveReservation();
                break;
            }
            case "13": {
                System.out.println("Restituer une réservation");
                mediathecaireController.deleteReservation();
                break;
            }
            case "14": {
                System.out.println("Afficher toutes les réservations d'un adherent");
                mediathecaireController.ShowAllAdherentReservations();
                break;
            }
            case "15": {
                System.out.println("Affichage de toutes les emprunts d'un adherent :");
                mediathecaireController.ShowAllAdherentEmprunts();
                break;
            }
            case "16": {
                System.out.println("Affichage des réservations hors délai : ");
                mediathecaireController.ShowAllReservationsOutOfTime();
                break;
            }
            case "17": {
                System.out.println("Affichage des emprunts hors délai : ");
                mediathecaireController.ShowAllEmpruntsOutOfTime();
                break;
            }
            case "18": {
                System.out.println("Restitution des réservation hors délai ");
                mediathecaireController.deleteEmpruntsOutOfTime();
                break;
            }


            case "0": {
                System.out.println("Fermeture de l'application...");
                break;
            }

            default: {
                System.out.println("Saisie invalide!  réessayez");
                break;
            }
        }

        if(!choice.equalsIgnoreCase("0")) {
            System.out.print("Appuyer sur une touche pour continuer...");
            AppView.scanner.nextLine();
        }
    }

    public void displayAllAdherents(List<Adherent> adherents) {
        for(Adherent adherent : adherents) {
            System.out.println("--------------------");
            System.out.println(adherent.toString());
            System.out.println();
        }

    }

    public String promptAdherantEmail() {
        System.out.println("Email de l'adherent : ");
        String email = AppView.scanner.nextLine();

        return email;
    }

    public String promptChoiceSearch() {
        String choice = "";
        boolean isNotGood= false;

        do {
            System.out.println("Choisir le mode de recherche : ");
            System.out.println("\t [1] - recherche par id emprunt/ reservation");
            System.out.println("\t [2] - recherche par id ouvrage");
            choice = AppView.scanner.nextLine();

            switch (choice) {
                case "1": {
                    System.out.println("\t Vous avez choisi le mode de recherche par id emprunt/ reservation");
                    break;
                }

                case "2": {
                    System.out.println("\t Vous avez choisi le mode de recherche par id ouvrage");
                    break;
                }
                default: {
                    System.out.println("Choix invalide! resseayer");
                    isNotGood = true;
                    break;
                }
            }

        } while(isNotGood);

        return choice;
    }
    public Long promptID() {

        System.out.println("Saisir l'ID : ");
        Long id = Long.parseLong(AppView.scanner.nextLine());
        return id;
    }
    public void displayAllEmprunt(List<Emprunt> objs) {
        for(Emprunt obj : objs) {
            System.out.println("--------------------");
            System.out.println(obj.getDateEmprunt() + "\t" + obj.getAdherent().getEmail() + "\t" + obj.getOuvrage().getId() + "\t" +obj.getOuvrage().getOeuvre().getTitre() );
            System.out.println();
        }

    }

    public void displayAllReservation(List<Reservation> objs) {
        for(Reservation obj : objs) {
            System.out.println("---------------------");
            System.out.println(obj.getDateReservation() + "\t" + obj.getAdherent().getEmail() + "\t" + obj.getOuvrage().getId() + "\t" +obj.getOuvrage().getOeuvre().getTitre() );
            System.out.println();
        }

    }

    public void NotAvailabaleOuvrage(Ouvrage ouvrage) {
        System.out.println("l'ouvrage selectioné n'est pas disponible");
    }

    public void displayAllOuvrages(List<Ouvrage> ouvragesdispo) {
        for(Ouvrage obj : ouvragesdispo) {
            System.out.println("Ouvrages disponible : ");
            System.out.println("----------------------");
            System.out.println(obj.getId() + "\t" + obj.getOeuvre().getTitre() + "\t" + obj.getOeuvre().getPrix());
            System.out.println();
        }
    }
    public Date promptDateRE() {
        System.out.println("Saisir la date yyyy-mm-jj: ");
        String date = AppView.scanner.nextLine();
        LocalDate dateEmp = LocalDate.parse(date, Constantes.formatter);
        Date dateEmprunt= Date.valueOf(dateEmp);
        return dateEmprunt;
    }

    public void notFind() {
        System.out.println("aucune donnée trouvée ");
    }
}
